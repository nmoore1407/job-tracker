<?php
/*
 * User: nick_moore
 * Date: 10/2/17
 * Time: 11:19 AM
 * Description: Job flow tracker for work
 */
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Job Tracker</title>

        <!-- Font Stylesheet -->
        <link href="https://fonts.googleapis.com/css?family=Antic|Comfortaa" rel="stylesheet">

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Page Specific CSS -->
        <link href="css/index.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- PHP Code start -->
        <?php
            // Connect to database
            $configs = include 'assets/.config.php';
            $un = $configs['username'];
            $pw = $configs['password'];
            try
            {
                $host = $configs['host'];
                $db = $configs['database'];
                $conn = new PDO("mysql:host=$host;dbname=$db", $un, $pw);
                // set the PDO error mode to exception
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                // echo "Connected successfully";
            }
            catch(PDOException $e)
            {
                print "Connection failed: ";
                print '<pre>';
                print_r($e);
                print '<pre>';
            }

            // Insert data into the database
            $insert_statement = 'INSERT INTO jobTrack( custCode, pattName, recvd) VALUES (:newCust, :newPatt, :dateIn)';

            if(($_POST['newCust']) != '')
            {
                $dateIn = date("Y-m-d");
                $insert_parmas = array(':newCust' => $_POST['newCust'], ':newPatt' => $_POST['newPatt'], ':dateIn'=> $dateIn);

                try
                {
                    // insert new row

                    $insert = $conn->prepare($insert_statement);
                    $insert->execute($insert_parmas);
                    $last_id = $conn->lastInsertId();
                }
                catch(PDOException $e)
                {
                    print "Query failed: ";
                    print '<pre>';
                    print_r($e);
                    print '<pre>';
                }
            }

            // Pull the data from the database for the table
            $result = $conn->query('SELECT * FROM jobTrack WHERE toDIG IS NULL');
            $items = $result->fetchAll();
            $colcount = $result->columnCount();
            $rowCount = $result->rowCount();

            // Get data that is less than the current date
            $resDate = $conn->query('SELECT * FROM jobTrack WHERE toCSR < CURDATE() AND fromCSR IS NULL AND toDIG IS NULL');
            $dateItems = $resDate->fetchAll();
            $rowCountDate = $resDate->rowCount();
        ?>

    </head>

    <body>
    <!-- /.container -->
        <div class="container header">
            <div class="row">
                <div class="col-lg-10 topHeader">
                    <h1>Job Tracker</h1>
                    <h5>(ALPHA)</h5>
                </div>
                <div class="col-lg-1 menuIconsLeft">
                    <a href="php/email.php"><img src="images/icons-v1-email.png" class="emailIcon" alt="Email icon" width="50px" height="auto"></a>
                </div>
                <div class="col-lg-1 menuIconsRight">
                   <a href="php/print.php"><img src="images/icons-v1-print.png" class="printIcon" alt="Print icon" width="50px" height="auto"></a>
                </div>
            </div>
        </div>
        <div class="container menu">
            <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-12 updateBtn">
                        <label for="submit-form" tabindex="0">
                            Update
                        </label>
                    </div>
                <a href="#" data-toggle="modal" data-target="#notifModal">
                    <div class="col-lg-3 col-md-3 col-sm-12 notifBtn">
                        <div class="col-sm-8 col-xsm-8">
                            <h3>Notifications</h3>
                        </div>
                        <div class="col-sm-4 col-xsm-4">
                            <h5 class="notNumbers">
                                <?php
                                    echo $rowCountDate;
                                ?>
                            </h5>
                        </div>
                    </div>
                </a>
                <div class="col-lg-7 col-md-7 col-sm-12 searchBox">
                    <form class="form-group" action="php/results.php" method="post">
                        <input type="text" class="form-control searchInput" name="searchItem" placeholder="Search Customer">
                        <input type="image" src="images/searchGlass.svg" class="searchIcon" alt="Submit Form">
                    </form>
                </div>
            </div>
        </div>
        <div class="container newInput">
            <div class="row">
                <div class="col-lg-1">
                    <!-- Empty for styling -->
                </div>
                <div class="col-lg-10">
                    <form class="form-group formNew" id="newForm" action="<?php print $_SERVER['PHP_SELF'] ?>" method="post">
                        <h4>New:</h4>
                        <input type="text" class="form-control new" name="newCust" placeholder="Customer">
                        <input type="text" class="form-control new" name="newPatt" placeholder="New Pattern">
                        <input type="submit" class="form-control submitBtn" value="Submit">
                        <input type="reset" class="form-control resetBtn" value="Clear">
                    </form>
                </div>
                <div class="col-lg-1">
                    <!-- Empty for styling -->
                </div>
            </div>
        </div>
        <div class="container output">
            <div class="row">
                <div class="col-lg-12 tableOutput">
                    <form class="form-group" action="php/update.php" method="post">
                        <table class="table-striped">
                            <tbody>
                            <tr>
                                <td colspan="6"></td>
                                <td><input type="reset" class="resetMainForm" value="Clear Selection"></td>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>Customer</th>
                                <th>Pattern Name</th>
                                <th>Received Date</th>
                                <th>To CSR</th>
                                <th>From CSR</th>
                                <th>To Digital</th>
                            </tr>
                            <?php
                                // Display the table data
                                foreach($items as $item) {
                                    echo('<tr>');
                                    echo('<td>' . $item['id'] . '</td>');
                                    echo('<td>' . $item['custCode'] . '</td>');
                                    echo('<td>' . $item['pattName'] . '</td>');
                                    echo('<td>' . $item['recvd'] . '</td>');

                                    // Check to see if there is a date set
                                    if ($item['toCSR'] != ''){
                                        echo('<td>' . $item['toCSR'] . '</td>');
                                    } else {
                                        echo('<td><input type="checkbox" name="ckToCSR[]" value="'. $item['id'] . '"></td>');
                                    }
                                    if ($item['fromCSR'] != ''){
                                        echo('<td>' . $item['fromCSR'] . '</td>');
                                    } else {
                                        echo('<td><input type="checkbox" name="ckFromCSR[]" value="'. $item['id'] . '"></td>');
                                    }
                                    if ($item['toDIG'] != ''){
                                        echo('<td>' . $item['toDIG'] . '</td>');
                                    } else {

                                        echo('<td><input type="checkbox" name="ckToDIG[]" value="'. $item['id'] . '"></td>');
                                    }
                                    echo('</tr>');
                                }
                            ?>
                            <tr>
                                <td><input type="submit" id="submit-form" class="hidden"></td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    <div id="notifModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Notifications</h4>
                </div>
                <div class="modal-body">
                    <form class="form-group" action="php/update.php" method="post">
                        <table class="table-striped">
                            <tbody>
                            <tr>
                                <td colspan="2"><input type="submit" class="form-control" value="Update"></td>
                                <td colspan="4"></td>
                                <td><input type="reset" class="resetMainForm" value="Clear Selection"></td>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>Customer</th>
                                <th>Pattern Name</th>
                                <th>Received Date</th>
                                <th>To CSR</th>
                                <th>From CSR</th>
                                <th>To Digital</th>
                            </tr>
                            <?php
                            // Display the table data
                            foreach($dateItems as $item) {
                                echo('<tr>');
                                echo('<td>' . $item['id'] . '</td>');
                                echo('<td>' . $item['custCode'] . '</td>');
                                echo('<td>' . $item['pattName'] . '</td>');
                                echo('<td>' . $item['recvd'] . '</td>');

                                // Check to see if there is a date set
                                if ($item['toCSR'] != ''){
                                    echo('<td>' . $item['toCSR'] . '</td>');
                                } else {
                                    echo('<td><input type="checkbox" name="ckToCSR[]" value="'. $item['id'] . '"></td>');
                                }
                                if ($item['fromCSR'] != ''){
                                    echo('<td>' . $item['fromCSR'] . '</td>');
                                } else {
                                    echo('<td><input type="checkbox" name="ckFromCSR[]" value="'. $item['id'] . '"></td>');
                                }
                                if ($item['toDIG'] != ''){
                                    echo('<td>' . $item['toDIG'] . '</td>');
                                } else {

                                    echo('<td><input type="checkbox" name="ckToDIG[]" value="'. $item['id'] . '"></td>');
                                }
                                echo('</tr>');
                            }
                            ?>
                            <tr>
                                <td><input type="submit" id="submit-form" class="hidden"></td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/index.js"></script>
    </body>

</html>
