<?php
header("refresh:5;url=../index.php");
echo('You will be redirected in approx. 5 seconds, if not click <a href="../index.php">here</a>');
/*
 * User: nick_moore
 * Date: 10/5/17
 * Time: 1:29 PM
 * Description: Page to update the database records, with redirect.
 */
?>

<?php
// Connect to database
    $configs = include '../assets/.config.php';
    $un = $configs['username'];
    $pw = $configs['password'];
    try
    {
        $host = $configs['host'];
        $db = $configs['database'];
        $conn = new PDO("mysql:host=$host;dbname=$db", $un, $pw);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // echo "Connected successfully";
    }
    catch(PDOException $e)
    {
        print "Connection failed: ";
        print '<pre>';
        print_r($e);
        print '<pre>';
    }

// Get the array created by the checkboxes in the form database display
    $updateToCSR = $_POST['ckToCSR'];
    $updateFromCSR = $_POST['ckFromCSR'];
    $updateToDIG = $_POST['ckToDIG'];

    if (isset($_POST['ckToCSR']))
    {
        $update_statement = 'UPDATE jobTrack SET toCSR = :dateToCSR WHERE id = :id';

        foreach ($_POST['ckToCSR'] as $key => $value) {
            $value = (int)$value;

            try {
                $update = $conn->prepare($update_statement);
                $dateToCSR = date("Y-m-d");
                $update_parmas = array(':id' => $value, ':dateToCSR' => $dateToCSR);
                $update->execute($update_parmas);
                echo('<h3>Record ' . $value . ' To CSR updated</h3>');
            }
            catch(PDOException $e) {
                print "Query failed: ";
                print '<pre>';
                print_r($e);
                print '<pre>';
            }
        }


    }

    if (isset($_POST['ckFromCSR']))
    {
        $update_statement = 'UPDATE jobTrack SET fromCSR = :dateFromCSR WHERE id = :id';

        foreach ($_POST['ckFromCSR'] as $key => $value) {
            $value = (int)$value;

            try {
                $update = $conn->prepare($update_statement);
                $dateFromSCR = date("Y-m-d");
                $update_parmas = array(':id' => $value, ':dateFromCSR' => $dateFromSCR);
                $update->execute($update_parmas);
                echo('<h3>Record ' . $value . ' From CSR updated</h3>');
            }
            catch(PDOException $e) {
                print "Query failed: ";
                print '<pre>';
                print_r($e);
                print '<pre>';
            }
        }


    }

    if (isset($_POST['ckToDIG']))
    {
        $update_statement = 'UPDATE jobTrack SET toDIG = :dateToDIG WHERE id = :id';

        foreach ($_POST['ckToDIG'] as $key => $value) {
            $value = (int)$value;

            try {
                $update = $conn->prepare($update_statement);
                $dateToDIG = date("Y-m-d");
                $update_parmas = array(':id' => $value, ':dateToDIG' => $dateToDIG);
                $update->execute($update_parmas);
                echo('<h3>Record ' . $value . ' To DIG updated</h3>');
            }
            catch(PDOException $e) {
                print "Query failed: ";
                print '<pre>';
                print_r($e);
                print '<pre>';
            }
        }


    }
?>
