<?php
/**
 * Created by PhpStorm.
 * User: nick_moore
 * Date: 10/4/17
 * Time: 6:31 PM
 * Description: Search Results
 */
?>

<?php
/**
 * Created by PhpStorm.
 * User: nick_moore
 * Date: 10/2/17
 * Time: 11:19 AM
 * Description: Job flow tracker for work
 */
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Job Tracker</title>

        <!-- Font Stylesheet -->
        <link href="https://fonts.googleapis.com/css?family=Antic|Comfortaa" rel="stylesheet">

        <!-- Bootstrap -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- Page Specific CSS -->
        <link href="../css/index.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- PHP Code start -->
        <?php
        // Connect to database
        $configs = include '../assets/.config.php';
        $un = $configs['username'];
        $pw = $configs['password'];
        try
        {
            $host = $configs['host'];
            $db = $configs['database'];
            $conn = new PDO("mysql:host=$host;dbname=$db", $un, $pw);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // echo "Connected successfully";
        }
        catch(PDOException $e)
        {
            print "Connection failed: ";
            print '<pre>';
            print_r($e);
            print '<pre>';
        }

        $search_statement = 'SELECT * FROM jobTrack WHERE custCode = :searchItem';
        $search_statement2 = 'SELECT * FROM jobTrack WHERE pattName = :searchItem';

        if(($_POST['searchItem']) != '')
        {
            $search_params = array(':searchItem' => $_POST['searchItem']);

            try
            {
                // insert new row
                $search = $conn->prepare($search_statement);
                $search->execute($search_params);
                $items = $search->fetchAll();
            }
            catch(PDOException $e)
            {
                print "Query failed: ";
                print '<pre>';
                print_r($e);
                print '<pre>';
            }
        }


        // Pull the data from the database for the table
        // $result = $conn->query($search_statement);
        // $items = $result->fetchAll();
        // $colcount = $result->columnCount();
        // $rowCount = $result->rowCount();
        ?>

    </head>

    <body>
        <!-- /.container -->
        <div class="container header">
            <div class="row">
                <div class="col-lg-12 topHeader">
                    <h1>Job Tracker | Search Results</h1>
                    <h5>(ALPHA)</h5>
                </div>
            </div>
        </div>
        <div class="container menu">
            <div class="row">
                <div class="col-lg-2 updateBtn">
                    <label for="submit-form" tabindex="0">
                        Update
                    </label>
                </div>
                <div class="col-lg-3">
                    <!-- Empty for styling -->
                </div>
                <div class="col-lg-7 searchBox">
                    <form class="form-group" action="<?php print $_SERVER['PHP_SELF']; ?>" method="post">
                        <input type="text" class="form-control searchInput" name="searchItem" placeholder="Search">
                        <input type="image" src="../images/searchGlass.svg" class="searchIcon" alt="Submit Form">
                    </form>
                </div>
            </div>
        </div>
        <div class="container output">
            <div class="row">
                <div class="col-lg-12 tableOutput">
                    <h6>Search for: <?php echo($_POST['searchItem']); ?></h6>
                    <form class="form-group" action="update.php" method="post">
                        <table class="table-striped">
                            <tbody>
                            <tr>
                                <td><a href="../index.php">Back</a> </td>
                                <td colspan="5"></td>
                                <td><input type="reset" class="resetMainForm" value="Clear Selection"></td>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>Customer</th>
                                <th>Pattern Name</th>
                                <th>Received Date</th>
                                <th>To CSR</th>
                                <th>From CSR</th>
                                <th>To Digital</th>
                            </tr>
                            <?php
                            // Display the table data
                            foreach($items as $item) {
                                echo('<tr>');
                                echo('<td>' . $item['id'] . '</td>');
                                echo('<td>' . $item['custCode'] . '</td>');
                                echo('<td>' . $item['pattName'] . '</td>');
                                echo('<td>' . $item['recvd'] . '</td>');

                                // Check to see if there is a date set
                                if ($item['toCSR'] != ''){
                                    echo('<td>' . $item['toCSR'] . '</td>');
                                } else {
                                    echo('<td><input type="checkbox" name="ckToCSR[]" value="'. $item['id'] . '"></td>');
                                }
                                if ($item['fromCSR'] != ''){
                                    echo('<td>' . $item['fromCSR'] . '</td>');
                                } else {
                                    echo('<td><input type="checkbox" name="ckFromCSR[]" value="'. $item['id'] . '"></td>');
                                }
                                if ($item['toDIG'] != ''){
                                    echo('<td>' . $item['toDIG'] . '</td>');
                                } else {

                                    echo('<td><input type="checkbox" name="ckToDIG[]" value="'. $item['id'] . '"></td>');
                                }
                                echo('</tr>');
                            }
                            ?>
                            <tr>
                                <td><input type="submit" id="submit-form" class="hidden"></td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/index.js"></script>
    </body>

</html>
