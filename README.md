# Job Tracker #

This is a general webapp / php program that reads a MySQL database and allows you to input, search, and update the records in the database.

### What is this repository for? ###

* General Job Tracker App
* Version 0.1 Alpha
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* This pulls the MySQL server information from a config.php folder.  Enter you info in the proper file.
* I run it on a localhost for testing purposes.
* config.php file (needs to be created), bootstrap files (included)
* Database configuration
* Run live tests inside of browser

### Contribution guidelines ###

* All help is appreciated.
* I want to see this grow into a full fledged Customer Service / Internal Application

### Who do I talk to? ###

* Nick (nmoore1407)
* At the moment no team is created.